@extends('frontend')

@section('styles')
@stop

@section('firstcontent')
 @stop

@section('content')
<div>
	<img src="{{URL::to('images/news.png')}}" class="newscaptain">
	<div class="capstain">
		<h1>Hồ sơ Người lao động</h1>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-4 user">
			<div class="panel panel-default">
				<div class="panel-heading">
					<img src="{{URL::to('images/user.png')}}" class="img-circle imguser" >
				</div>
				<div class="panel-body">
					<h5>{!!$user->username!!}</h5>
					<hr>
					<div class="row">
							<div class="col-md-3"><h5>Họ tên </h5>  </div>
							<div class="col-md-9"><h5>{!!$user->name!!}</h5></div>
					</div>
					<div class="row">
							<div class="col-md-3"><h5>Email </h5></div>
							<div class="col-md-9"> <h5>{!!$user->email!!} </h5></div>
					</div>
					
				</div>
			</div>
		</div>
		<div class="col-md-8">
			@include('layouts.calendarUser')
		</div>
	</div>
</div>
@stop

@section('scripts')
<script type="text/javascript"  src="{{URL::to('js/moment.js')}}"></script>
<link href="{{URL::to('css/fullcalendar.css')}}" rel='stylesheet' />
<link href="{{URL::to('css/fullcalendar.print.css')}}" rel='stylesheet' media='print' />
<script src="{{URL::to('js/fullcalendar.min.js')}}"></script>
<script src="{{URL::to('js/gcal.js')}}"></script>
<script src="{{URL::to('js/calendaruser.js')}}"></script>


@stop