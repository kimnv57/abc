<!DOCTYPE html>
<html lang="en">
     <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>@section('title') Laravel 5 Sample Site @show</title>
     <link rel="icon" href="images/favicon.ico">
     <link rel="shortcut icon" href="images/favicon.ico" />
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/bootstrap.min.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/mystyle.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/dish.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/footer.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/news.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/contacts.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/calendar/pikaday.css')}}">
     


     <!-- <link href="{{ asset('/css/all.css') }}" rel="stylesheet"> -->
     @yield('styles')
     

     </head>
<body>
  @include('partials.normal.nav')
  <div>
    @yield('content')
    
  </div>
  </body>
  <div class="my_footer">
      @include('partials.footer')
      @include('partials.footer-design')
  </div>
  
<script src="{{ URL::to('/js/jquery-1.9.1.min.js') }}"></script>
<script src="{{ URL::to('/js/bootstrap.min.js') }}"></script>
 <script type="text/javascript"  src="{{URL::to('js/pikaday.js')}}"></script>
<script type="text/javascript"  src="{{URL::to('js/calendar.js')}}"></script>
<script type="text/javascript"  src="{{URL::to('js/moment.js')}}"></script>

<script>
$(function(){



    $('.images1').hover(function(){
        $('.cs-carousel-body1').css('bottom','0px');
    },function(){
        $('.cs-carousel-body1').css('bottom', '-80px');
    });
    $('.images2').hover(function(){
        $('.cs-carousel-body2').css('bottom','0px');
    },function(){
        $('.cs-carousel-body2').css('bottom', '-80px');
    });
    $('.images3').hover(function(){
        $('.cs-carousel-body3').css('bottom','0px');
    },function(){
        $('.cs-carousel-body3').css('bottom', '-80px');
    });
    $('.previous').click(function(){
        $('.myrowfood > .active').next('div').find('.food').trigger('click');
    });

    $('.next').click(function(){
        $('.myrowfood > .active').prev('div').find('food').trigger('click');
    });


    $('#loginform').submit(function(event) {
      event.preventDefault();
      var form = $(this);
      $.ajax({
          type : form.attr('method'),
          url : form.attr('action'),
          data : form.serialize()
          }).success(function() {
            window.parent.location.replace("/abckitchen1/public/auth/login");
        })
          .fail(function(jqXHR, textStatus, errorThrown) {
              // Optionally alert the user of an error here...
              var textResponse = jqXHR.responseText;
              var alertText = "";
              var jsonResponse = jQuery.parseJSON(textResponse);
              $.each(jsonResponse, function(n, elem) {
                  alertText = alertText + elem + "<br>";
              });
              $(".loi").html('');
              $("<div>").attr("class","alert").attr("class","alert-danger").appendTo($(".loi"));
              $("<strong>").html("One of the following conditions is not met:").appendTo($(".alert-danger"));
              $("<br>").appendTo($(".alert-danger"));
              $("<strong>").html(alertText).appendTo($(".alert-danger"));
          });
        });

    $('#loginform2').submit(function(event) {
      event.preventDefault();
      var form = $(this);
      $.ajax({
          type : form.attr('method'),
          url : form.attr('action'),
          data : form.serialize()
          }).success(function() {
            window.parent.location.replace("/abckitchen1/public/resignfood");
        })
          .fail(function(jqXHR, textStatus, errorThrown) {
              // Optionally alert the user of an error here...
              var textResponse = jqXHR.responseText;
              var alertText = "";
              var jsonResponse = jQuery.parseJSON(textResponse);
              $.each(jsonResponse, function(n, elem) {
                  alertText = alertText + elem + "<br>";
              });
              $(".loi").html('');
              $("<div>").attr("class","alert").attr("class","alert-danger").appendTo($(".loi"));
              $("<strong>").html("One of the following conditions is not met:").appendTo($(".alert-danger"));
              $("<br>").appendTo($(".alert-danger"));
              $("<strong>").html(alertText).appendTo($(".alert-danger"));
          });
        });
});
</script>
@yield('scripts')


</html>