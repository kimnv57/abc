@extends('admin.default')

{{-- Web site Title --}}
@section('title') {{{ Lang::get("admin/users.users") }}} :: @parent
@stop
@section('styles')
@parent
    <!-- css for calendar -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('css/calendar/pikaday.css')}}">

@stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>
            Thực đơn
            
        </h3>
    </div>
    <div class="row">
        <!-- calendar -->
        <div class="col-md-3">
            <input type="text" id="myCalendar">
        </div>
        <!-- list food -->
        <div class="col-md-9">
            <div class="pull-right">
                <div class="pull-right">
                    <a id="new" href="{{{ URL::to('admin/menus/create/2015-05-08') }}}"
                       class="btn btn-sm  btn-primary iframe"><span
                                class="glyphicon glyphicon-plus-sign"></span> {{
                    Lang::get("admin/modal.new") }}</a>
                </div>
            </div>
            <div class="menu"></div>

        </div>
    </div>

@stop

{{-- Scripts --}}
@section('scripts')
    @parent

    <!-- js for calendar -->
    <script type="text/javascript"  src="{{URL::to('js/moment.js')}}"></script>
    <script type="text/javascript"  src="{{URL::to('js/moment-recur.js')}}"></script>
    <script type="text/javascript"  src="{{URL::to('js/pikaday.js')}}"></script>
    <script type="text/javascript"  src="{{URL::to('js/calendaradmin.js')}}"></script>

@stop
