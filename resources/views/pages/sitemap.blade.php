@extends('frontend')
@section('styles')
@stop

@section('content')

<div>
	<img src="{{URL::to('images/news.png')}}" class="newscaptain">
	<div class="capstain">
		<h1>Bản Đồ Website</h1>
	</div>
</div>
<div class="container sitemap">
		<ul>
			<a href="{{URL::to('')}}">
				<h5>Trang chủ</h5>
			</a>
			<li><p>
				Hiển thị cho người dùng mảng tin tức nổi bật và các món ăn nổi bật
			</p></li>
		</ul>
		<ul>
			<a href="{{URL::to('/resignfood')}}">
				<h5>Đăng kí ăn</h5>
			</a>
			<li><p>
				Tab đăng kí ăn của người lao động
			</p></li>
		</ul>
		<ul>
			<a href="{{URL::to('/news')}}">
				<h5>Bản tin</h5>
			</a>
			<li><p>
				Tab tin tức của nhà ăn ABC Kitchen
			</p></li>
		</ul>
		<ul>
			<a href="{{URL::to('/contact')}}">
				<h5>Liên hệ</h5>
			</a>
			<li><p>
				Tab liên hệ của người dùng tới nhà ăn
			</p></li>
		</ul>
		<ul>
			<a href="{{URL::to('/about')}}">
				<h5>Giới thiệu</h5>			
			</a>
			<li><p>
					Tab giới thiệu thông tin về nhà ăn
			</p></li>
		</ul>
		<ul>
			<a href="">
				<h5>Hồ sơ người lao động</h5>
			</a>
			<li><p>
				Trang thông tin người lao động, theo dõi ăn hàng tháng của người lao động
			</p></li>
		</ul>
</div>
 @stop

 @section('scripts')

@stop