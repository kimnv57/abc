// for resign food
$(function(){
	
	$('#mydatePicker').change(function() {


		var currentday = moment($("#mydatePicker").val()).format("YYYY-MM-DD");
		var today = moment(new Date()).format("YYYY-MM-DD");

		$('#resigndate').attr('value',currentday);
		$('.todaydate').attr('value',today);
		$('#destroydate').attr('value',currentday);
        var link ='http://localhost/abckitchen1/public/menus/' + currentday + '/data';
        window.history.pushState({path:currentday},'',	currentday);
	    $.ajax({
	        url : link,
	        type : 'GET',
	        dataType:'json',
	        success : function(data) {
	            var items = [];
	            var sum=0;
	            $(".menu").html("");
	            $( "#currentDate" ).html(currentday);
	            var dishes = data.dishes;
	            $.each( dishes, function( i, item ) {
	                sum+=dishes[i].price;
	                items.push( "<tr>");
	                items.push( "<td><a href=\"\">"+"<img src=\"http://localhost/abckitchen1/public/appfiles/dishimages/thumbs/"+dishes[i].image+"\" class=\"img-circle\">"+"</a></td>");
	                items.push( "<td>"+dishes[i].name+"</td>");
	                items.push( "<td>"+dishes[i].price+" Đ</td>");
	                items.push( "</tr>");
	              });
	            items.push( "<tr>");
                items.push( "<td></td>");
                items.push( "<td><p>Tong tien</p></td>");
                items.push( "<td>"+sum+" Đ</td>");
                items.push( "</tr>");
                if(data.check==true) {
                	$("#buttonResign").hide();
                	$("#buttonDestroy").show();
                } else {
                	$("#buttonResign").show();
                	$("#buttonDestroy").hide();
                }
                
                $(".tableHeader").show();
	            $( ".menu").html(items.join(""));
	            
	        },
	        error : function(request,error)
	        {
	            $(".menu").html("");
	            $( "#currentDate" ).html(currentday);
	            $( "<h3>" ).attr( "class", "notice").html("Chưa có thực đơn").appendTo( ".menu" );
	            $(".tableHeader").hide();
	            $("#buttonResign").hide();
	            $("#buttonDestroy").hide();
	        }
        });

	});


	//resign food form submit
	$('#resignform').submit(function(event) {
		event.preventDefault();
		var form = $(this);
		$.ajax({
			type : form.attr('method'),
			url : form.attr('action'),
			data : form.serialize()
			})
			.success(function() {
				$("#buttonResign").hide();
	            $("#buttonDestroy").show();
				$('.btn-primary').click();
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
                alert("Bạn Chỉ Được Đăng Ký Trước Khi Ăn 24h!");
                $('.btn-primary').click();
            });
	});

	$('#cancelform').submit(function(event) {
		event.preventDefault();
		var form = $(this);
		$.ajax({
			type : form.attr('method'),
			url : form.attr('action'),
			data : form.serialize()
			})
			.success(function() {
				$("#buttonResign").show();
	            $("#buttonDestroy").hide();
				$('.btn-primary').click();
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				alert("Bạn Chỉ Được Hủy Trước Khi Ăn 24h!");
				$('.btn-primary').click();
                
            });
	});
});





function mysetdate(date) {
	var currentday;
	if(date==''||date==null) {
		document.getElementById("mydatePicker").valueAsDate = new Date();
		currentday = moment(new Date()).format("YYYY-MM-DD");
		window.history.pushState({path:'resignfood/'+currentday},'','resignfood/'+currentday);
	}
	else {
		currentday = date;
		document.getElementById("mydatePicker").valueAsDate = new Date(date);
		window.history.pushState({path:currentday},'',currentday);
	}
	var today = moment(new Date()).format("YYYY-MM-DD");
	$('.todaydate').attr('value',today);
	$('#resigndate').attr('value',currentday);
	$('#destroydate').attr('value',currentday);
	
	
    var link ='http://localhost/abckitchen1/public/menus/' + currentday + '/data';
    
	$.ajax({
	        url : link,
	        type : 'GET',
	        dataType:'json',
	        success : function(data) {
	            var items = [];
	            var sum=0;
	            $(".menu").html("");
	            $( "#currentDate" ).html(currentday);
	            var dishes = data.dishes;
	            $.each( dishes, function( i, item ) {
	                sum+=dishes[i].price;
	                items.push( "<tr>");
	                items.push( "<td><a href=\"#\">"+"<img src=\"http://localhost/abckitchen1/public/appfiles/dishimages/thumbs/"+dishes[i].image+"\" class=\"img-circle\">"+"</a></td>");
	                items.push( "<td>"+dishes[i].name+"</td>");
	                items.push( "<td>"+dishes[i].price+" Đ</td>");
	                items.push( "</tr>");
	              });
	            items.push( "<tr>");
                items.push( "<td></td>");
                items.push( "<td><p>Tong tien</p></td>");
                items.push( "<td>"+sum+" Đ</td>");
                items.push( "</tr>");

                 if(data.check==true) {
                	$("#buttonResign").hide();
                	$("#buttonDestroy").show();
                } else {
                	$("#buttonResign").show();
                	$("#buttonDestroy").hide();
                }
                $(".tableHeader").show();
	            $( ".menu").html(items.join(""));
	            
	        },
	        error : function(request,error)
	        {
	            $(".menu").html("");
	            $( "#currentDate" ).html(currentday);
	            $( "<h3>" ).attr( "class", "notice").html("Chưa có thực đơn").appendTo( ".menu" );
	            $(".tableHeader").hide();
	            $("#buttonResign").hide();
	            $("#buttonDestroy").hide();
	        }
        });



}