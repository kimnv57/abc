<?php

use App\User_Menu;
use Illuminate\Database\Seeder;

class Users_MenusTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users_menus')->delete();

        $item = new User_Menu;
        $item->user_id=1;
        $item->menu_id=1;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=1;
        $item->menu_id=5;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=1;
        $item->menu_id=8;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=1;
        $item->menu_id=3;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=1;
        $item->menu_id=7;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=2;
        $item->menu_id=1;
        $item->eat=false;
        $item->save();

         $item = new User_Menu;
        $item->user_id=2;
        $item->menu_id=6;
        $item->eat=false;
        $item->save();

         $item = new User_Menu;
        $item->user_id=2;
        $item->menu_id=4;
        $item->eat=false;
        $item->save();

         $item = new User_Menu;
        $item->user_id=2;
        $item->menu_id=8;
        $item->eat=false;
        $item->save();

         $item = new User_Menu;
        $item->user_id=2;
        $item->menu_id=5;
        $item->eat=false;
        $item->save();

         $item = new User_Menu;
        $item->user_id=3;
        $item->menu_id=1;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=3;
        $item->menu_id=8;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=3;
        $item->menu_id=4;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=3;
        $item->menu_id=5;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=3;
        $item->menu_id=7;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=4;
        $item->menu_id=1;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=4;
        $item->menu_id=5;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=4;
        $item->menu_id=6;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=4;
        $item->menu_id=7;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=4;
        $item->menu_id=8;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=5;
        $item->menu_id=1;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=5;
        $item->menu_id=2;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=5;
        $item->menu_id=3;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=5;
        $item->menu_id=4;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=6;
        $item->menu_id=2;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=6;
        $item->menu_id=5;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=6;
        $item->menu_id=2;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=6;
        $item->menu_id=6;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=6;
        $item->menu_id=7;
        $item->eat=false;
        $item->save();

        $item = new User_Menu;
        $item->user_id=6;
        $item->menu_id=8;
        $item->eat=false;
        $item->save();
    }

}