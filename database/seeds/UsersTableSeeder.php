<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy

class UsersTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();
		\App\User::create([
			'name' => 'Admin User',
			'username' => 'admin_user',
			'email' => 'admin@gmail.com',
			'password' => bcrypt('12345'),
		]);

		\App\User::create([
			'name' => 'Test User',
			'username' => 'normal_user',
			'email' => 'accountant@gmail.com',
			'password' => bcrypt('12345'),
		]);

		\App\User::create([
			'name' => 'Nguyễn Thị Thảo',
			'username' => 'thaont_94',
			'email' => 'nguyenthao838@gmail.com',
			'password' => bcrypt('123456'),
		]);

		\App\User::create([
			'name' => 'Nguyễn Văn Kim',
			'username' => 'kimnv_94',
			'email' => 'kimnv94@gmail.com',
			'password' => bcrypt('abcxyz'),
		]);

		\App\User::create([
			'name' => 'Hoàng Văn Kiệt',
			'username' => 'kiethv_89',
			'email' => 'kiethv_89@gmail.com',
			'password' => bcrypt('123456'),
		]);

		\App\User::create([
			'name' => 'Hoàng Văn Khải',
			'username' => 'Khaihv_90',
			'email' => 'khaihv90@gmail.com',
			'password' => bcrypt('123456'),
		]);

	}

}
