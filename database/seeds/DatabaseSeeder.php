<?php
use App\Menu;
use App\AssignedDishes;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        // Add calls to Seeders here
       
         $this->call('UsersTableSeeder');
         $this->call('RolesTableSeeder');
         $this->call('DishesTableSeeder');
         $this->call('NewssTableSeeder');
         $this->call('FuelsTableSeeder');
         //  $this->call('MenusTableSeeder');
        
         DB::table('menus')->delete();
         DB::table('dishes_menus')->delete();

        $menu = new Menu;
        $menu->eat_time = new DateTime('2015-05-02');
        $menu->price=70000;
        $menu->save();

        $menu = new Menu;
        $menu->eat_time = new DateTime('2015-05-03');
        $menu->price=60000;
        $menu->save();

        $menu = new Menu;
        $menu->eat_time = new DateTime('2015-05-04');
        $menu->price=58000;
        $menu->save();

        $menu = new Menu;
        $menu->eat_time = new DateTime('2015-05-05');
        $menu->price=49000;
        $menu->save();

        $menu = new Menu;
        $menu->eat_time = new DateTime('2015-06-05');
        $menu->price=41000;
        $menu->save();

        $menu = new Menu;
        $menu->eat_time = new DateTime('2015-06-06');
        $menu->price=47000;
        $menu->save();

        $menu = new Menu;
        $menu->eat_time = new DateTime('2015-06-07');
        $menu->price=42000;
        $menu->save();

        $menu = new Menu;
        $menu->eat_time = new DateTime('2015-06-08');
        $menu->price=53000;
        $menu->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 1;
        $assigneddish->dish_id = 1;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 1;
        $assigneddish->dish_id = 2;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 1;
        $assigneddish->dish_id = 3;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 1;
        $assigneddish->dish_id = 22;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 1;
        $assigneddish->dish_id = 23;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 1;
        $assigneddish->dish_id = 11;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 2;
        $assigneddish->dish_id = 3;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 2;
        $assigneddish->dish_id = 4;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 2;
        $assigneddish->dish_id = 22;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 2;
        $assigneddish->dish_id = 6;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 2;
        $assigneddish->dish_id = 10;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 2;
        $assigneddish->dish_id = 30;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 3;
        $assigneddish->dish_id = 4;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 3;
        $assigneddish->dish_id = 22;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 3;
        $assigneddish->dish_id = 8;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 3;
        $assigneddish->dish_id = 11;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 3;
        $assigneddish->dish_id = 12;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 3;
        $assigneddish->dish_id = 13;
        $assigneddish->save();


        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 4;
        $assigneddish->dish_id = 1;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 4;
        $assigneddish->dish_id = 10;
        $assigneddish->save();
        
        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 4;
        $assigneddish->dish_id = 13;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 4;
        $assigneddish->dish_id = 22;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 4;
        $assigneddish->dish_id = 26;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 4;
        $assigneddish->dish_id = 19;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 4;
        $assigneddish->dish_id = 33;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 5;
        $assigneddish->dish_id = 1;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 5;
        $assigneddish->dish_id = 25;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 5;
        $assigneddish->dish_id = 30;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 5;
        $assigneddish->dish_id = 40;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 5;
        $assigneddish->dish_id = 22;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 5;
        $assigneddish->dish_id = 51;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 6;
        $assigneddish->dish_id = 32;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 6;
        $assigneddish->dish_id = 40;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 6;
        $assigneddish->dish_id = 15;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 6;
        $assigneddish->dish_id = 16;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 6;
        $assigneddish->dish_id = 10;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 6;
        $assigneddish->dish_id = 22;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 7;
        $assigneddish->dish_id = 41;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 7;
        $assigneddish->dish_id = 1;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 7;
        $assigneddish->dish_id = 18;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 7;
        $assigneddish->dish_id = 11;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 7;
        $assigneddish->dish_id = 22;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 7;
        $assigneddish->dish_id = 23;
        $assigneddish->save();


        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 8;
        $assigneddish->dish_id = 12;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 8;
        $assigneddish->dish_id = 10;
        $assigneddish->save();
        
        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 8;
        $assigneddish->dish_id = 18;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 8;
        $assigneddish->dish_id = 13;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 8;
        $assigneddish->dish_id = 26;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 8;
        $assigneddish->dish_id = 19;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 8;
        $assigneddish->dish_id = 22;
        $assigneddish->save();
        $this->call('Users_MenusTableSeeder');
	}
}
