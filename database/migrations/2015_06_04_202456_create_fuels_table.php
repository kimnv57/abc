<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fuels', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->float('quantity');
			$table->integer('price');
			$table->integer('total');
			$table->integer('fueldaily_id')->unsigned();
			$table->foreign('fueldaily_id')->references('id')->on('fueldaily')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fuels');
	}

}
