<?php
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {
	Route::pattern('id', '[0-9]+');
    Route::pattern('id2', '[0-9]+');

    #Admin Dashboard
    Route::get('dashboard', 'Admin\DashboardController@index');

    #Users
    Route::get('users/', 'Admin\UserController@index');
    Route::get('users/create', 'Admin\UserController@getCreate');
    Route::post('users/create', 'Admin\UserController@postCreate');
    Route::get('users/{id}/edit', 'Admin\UserController@getEdit');
    Route::post('users/{id}/edit', 'Admin\UserController@postEdit');
    Route::get('users/{id}/delete', 'Admin\UserController@getDelete');
    Route::post('users/{id}/delete', 'Admin\UserController@postDelete');
    Route::get('users/data', 'Admin\UserController@data');
    #Dishes
    Route::get('dishes/', 'Admin\DishController@index');
    Route::get('dishes/create', 'Admin\DishController@getCreate');
    Route::post('dishes/create', 'Admin\DishController@postCreate');
    Route::get('dishes/{id}/edit', 'Admin\DishController@getEdit');
    Route::post('dishes/{id}/edit', 'Admin\DishController@postEdit');
    Route::get('dishes/{id}/delete', 'Admin\DishController@getDelete');
    Route::post('dishes/{id}/delete', 'Admin\DishController@postDelete');
    Route::get('dishes/data', 'Admin\DishController@data');
    Route::get('dishes/dessertsdata', 'Admin\DishController@dessertsdata');
    Route::get('dishes/maindata', 'Admin\DishController@maindata');
    Route::get('dishes/soupdata', 'Admin\DishController@soupdata');

    #news
    Route::get('news/', 'Admin\NewsController@index');
    Route::get('news/create', 'Admin\NewsController@getCreate');
    Route::post('news/create', 'Admin\NewsController@postCreate');
    Route::get('news/{id}/edit', 'Admin\NewsController@getEdit');
    Route::post('news/{id}/edit', 'Admin\NewsController@postEdit');
    Route::get('news/{id}/delete', 'Admin\NewsController@getDelete');
    Route::post('news/{id}/delete', 'Admin\NewsController@postDelete');
    Route::get('news/data', 'Admin\NewsController@data');

    #fueldailys
    Route::get('fueldailys/', 'Admin\FuelDailyController@index');
    Route::get('fueldailys/create/{date}', 'Admin\FuelDailyController@getCreate');
    Route::post('fueldailys/create', 'Admin\FuelDailyController@postCreate');
    Route::get('fueldailys/{id}/edit', 'Admin\FuelDailyController@getEdit');
    Route::post('fueldailys/{id}/edit', 'Admin\FuelDailyController@postEdit');
    Route::get('fueldailys/{id}/delete', 'Admin\FuelDailyController@getDelete');
    Route::post('fueldailys/{id}/delete', 'Admin\FuelDailyController@postDelete');
    Route::get('fueldailys/{date}/data', 'Admin\FuelDailyController@data');


    #menus
    Route::get('menus/', 'Admin\MenuController@index');
    Route::get('menus/create/{time}', 'Admin\MenuController@getCreate');
    Route::post('menus/create', 'Admin\MenuController@postCreate');
    Route::get('menus/{id}/edit', 'Admin\MenuController@getEdit');
    Route::post('menus/{id}/edit', 'Admin\MenuController@postEdit');
    Route::get('menus/{eat_time}/delete', 'Admin\MenuController@getDelete');
    Route::post('menus/{eat_time}/delete', 'Admin\MenuController@postDelete');
    
    #languages
    Route::get('languages/', 'Admin\LanguageController@index');


    #statistical
    Route::get('statisticals/{date}/data', 'Admin\StatisticalController@data');
    Route::post('statisticals/{date}/save', 'Admin\StatisticalController@save');
    Route::get('statisticals', 'Admin\StatisticalController@show');
});