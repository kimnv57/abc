<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\News;
use App\Http\Requests\Admin\NewsRequest;
use App\Http\Requests\Admin\NewsEditRequest;
use App\Http\Requests\Admin\DeleteRequest;
use Illuminate\Support\Facades\Input;
use App\Helpers\Thumbnail;
use Datatables;

class NewsController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        // Show the page
        return view('admin.newss.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        return view('admin.newss.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(newsRequest $request) {

        $news = new News ();
        $news -> title = $request->title;
        $news -> content = $request->content;
        $news -> type = $request->type;
        $picture = "";
        if($request->hasFile('image'))
        {
        
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file -> getClientOriginalExtension();
            $picture = sha1($filename . time()) . '.' . $extension;
        }
        
        $news -> image = $picture;
        $news -> save();
        if($request->hasFile('image'))
        {
            $destinationPath = public_path() . '/appfiles/newsimages/';
            $request->file('image')->move($destinationPath, $picture);

            $path2 = public_path() . '/appfiles/newsimages' . '/thumbs/';
            Thumbnail::generate_image_thumbnail($destinationPath . $picture, $path2 . $picture);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $news
     * @return Response
     */
    public function getEdit($id) {

        $news = News::find($id);

        return view('admin.newss.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $news
     * @return Response
     */
    public function postEdit(NewsEditRequest $request, $id) {

        $news = News::find($id);
        $news -> content = $request->content;
         $news -> type = $request->type;
        $picture = "";
        if($request->hasFile('image'))
        {
        
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file -> getClientOriginalExtension();
            $picture = sha1($filename . time()) . '.' . $extension;
            // add records to the log
            $news -> image = $picture;
        }
        $news -> save();
        if($request->hasFile('image'))
        {
            $destinationPath = public_path() . '/appfiles/newsimages/';
            $request->file('image')->move($destinationPath, $picture);

            $path2 = public_path() . '/appfiles/newsimages' . '/thumbs/';
            Thumbnail::generate_image_thumbnail($destinationPath . $picture, $path2 . $picture);

        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param $news
     * @return Response
     */

    public function getDelete($id)
    {
        $news = news::find($id);
        // Show the page
        return view('admin.newss.delete', compact('news'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $news
     * @return Response
     */
    public function postDelete(DeleteRequest $request,$id)
    {
        $news= News::find($id);
        $news->delete();
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
       
    $newss = News::select(array('newss.id','newss.id','newss.image','newss.title', 'newss.content', 'newss.created_at'))->orderBy('newss.title', 'ASC');

        return Datatables::of($newss)
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/news/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ Lang::get("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'admin/news/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ Lang::get("admin/modal.delete") }}</a>
                ')
            ->edit_column('content','{{ substr($content, 0, 55) }}...')
            ->edit_column('image', '{!! ($image!="")? "<img style=\"max-width: 100px; max-height: 70px;\" src=\"../appfiles/newsimages/thumbs/$image\">":""; !!}')
            ->add_column('check','<input class="mycheckbox" type="checkbox" name="check[]" value="{{$id}}"/>')
            ->make(true);
    }

    public function show($id)
    {
        $news = news::find($id);
        // Show the page
        return view('newss.index', compact('news'));
    }

   
}
