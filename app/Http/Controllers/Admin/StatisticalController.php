<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Auth;
use Input;
use App\User_Menu;
use App\Menu;
use Datatables;


class StatisticalController extends AdminController {

	

	public function show()
	{
		return view('admin.statistical.index');
	}
	public function data($date)
	{
		$menu = Menu::where("eat_time","=",$date)->first();
		if($menu!=null) {
			$user_menus = User_Menu::join('users','users_menus.user_id','=','users.id')
			->where("users_menus.menu_id","=",$menu->id)->
			select(array('users_menus.id','users_menus.user_id','users.name','users.email','users_menus.eat'))->orderBy('users.name', 'ASC');

	        return Datatables::of($user_menus)
            ->add_column('check','@if ($eat==true) <input class="mycheckbox" type="checkbox" name="check[]" checked  value="{{$id}}"/>
            	@else <input class="mycheckbox" type="checkbox"  name="check[]" value="{{$id}}"/> @endif')
            ->make(true);
		}
		// return null set
			$user_menus = User_Menu::join('users','users_menus.user_id','=','users.id')
			->where("users_menus.menu_id","=",-1)->
			select(array('users_menus.id','users_menus.user_id','users.name','users.email'))->orderBy('users.name', 'ASC');

	        return Datatables::of($user_menus)
            ->add_column('check','<input class="mycheckbox" type="checkbox"  name="check[]" value="{{$id}}"/>')
            ->make(true);
		
	}
	// resign food
	public function save($date)
	{
		$values=Input::get('check');

		if($values!=null) {
			foreach($values as $value) {
				$user_menu = User_Menu::where("id","=",$value)->first();
				$user_menu->eat=true;
				$user_menu->save();
			}
		}
	}
}
